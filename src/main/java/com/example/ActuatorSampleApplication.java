package com.example;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ActuatorSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActuatorSampleApplication.class, args);
	}

}

@RestController
class PrintPodAddress {

	@Autowired
	RestTemplate restTemplate;

	private static final Logger LOG = Logger.getLogger(PrintPodAddress.class.getName());

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@RequestMapping(value = "/printpodaddress", method = RequestMethod.GET)
	public String printPodAddress() throws UnknownHostException {
		LOG.info("Inside printPodAddress..");
		return "Pod address in Staging test123--> " + InetAddress.getLocalHost().getHostAddress();
	}
}